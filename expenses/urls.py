from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('receipts/', include('receipts.urls')),
    path('', RedirectView.as_view(url='/receipts/', permanent=False)),
    path('accounts/', include(('accounts.urls', 'accounts'), namespace='accounts')),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
]
