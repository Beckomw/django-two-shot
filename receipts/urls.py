from django.urls import path
from .views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
    ExpenseCategoryCreateView,
    AccountCreateView
)

urlpatterns = [
    path('', ReceiptListView.as_view(), name='home'),  # This should be named 'home'
    path('create/', ReceiptCreateView.as_view(), name='create_receipt'),
    path('categories/', ExpenseCategoryListView.as_view(), name='category_list'),
    path('categories/create/', ExpenseCategoryCreateView.as_view(), name='create_category'),
    path('accounts/', AccountListView.as_view(), name='account_list'),
    path('accounts/create/', AccountCreateView.as_view(), name='create_account'),
]
