from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm, CategoryForm
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy






class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    form_class = AccountForm
    template_name = 'receipts/account_form.html'
    success_url = reverse_lazy('account_list') 
    
    def form_valid(self, form): 
        form.instance.owner = self.request.user
        return super().form_valid(form)
    



def create_account(request):
    form = AccountForm()
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    return render(request, 'receipts/account_form.html', {'form': form})



def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    return render(request, 'create_receipt.html', {'form': form})





class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory 
    form_class = ExpenseCategoryForm
    template_name = 'receipts/category_form.html'
    success_url = reverse_lazy('category_list')

    def form_valid(self, form):
        form.instance.owner = self.request.user 
        return super().form_valid(form)







class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    form_class = ReceiptForm
    template_name = 'receipts/receipt_form.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)
    


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/receipt_list.html'
    context_object_name = 'receipts'

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
    

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = 'receipts/category_list.html'
    context_object_name = 'categories' 

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

class AccountListView(LoginRequiredMixin, ListView):
    model = Account 
    template_name = 'receipts/account_list.html'
    context_object_name = 'accounts' 


    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
    
