from django.shortcuts import render, redirect 
from django.contrib.auth import authenticate, login, logout  
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User

# Create your views here.
def logout_view(request):
    logout(request)
    return redirect('login')

    


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request, request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')  
            else:
                form.add_error(None, 'Invalid username or password')
    else:
        form = LoginForm()

    return render(request, 'accounts/login.html', {'form': form})






def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

        if password != password_confirmation:
            form.add_error('password_confirmation', 'The passwords do not match')
        else:
            user = User.objects.create_user(username=username,password=password)
            user = authenticate(username=username,password=password)
            if user is not None: 
                login(request,user)
                return redirect('home')
    else:
        form = SignupForm()

    return render(request,'accounts/signup.html',{'form':form})





















